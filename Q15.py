
def mergeDictionary(dict_a, dict_b):
    """ Gets 2 dictionaries, merges them and returns the merged dictionary. """
    merged = dict(dict_a.items())
    for key in dict_b.keys():
        """ If the key is used in both dictionaries, merge the values into a list """
        if key in dict_a.keys():
            merged[key] = [dict_a[key], dict_b[key]]
        else:
            merged.update({key: dict_b[key]})
    return merged


