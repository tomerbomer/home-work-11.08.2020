dict_of_ids = {}
while True:
    id_num = input("Enter ID: ")
    if int(id_num) == -1:
        break
    if id_num in dict_of_ids.keys():
        print(f"The id {id_num} was already entered.")
        continue
    dict_of_ids[id_num] = input("Enter Full Name: ")
for idNum, name in dict_of_ids.items():
    print(f"{idNum}: {name}")




