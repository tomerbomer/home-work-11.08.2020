
def getSortedKeys(dictionary):
    """ Gets a dictionary and returns the keys - sorted ascending. """
    list_of_keys = [key for key in dictionary.keys()]
    list_of_keys.sort()
    return list_of_keys
