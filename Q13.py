
def tryGetValue(dictionary, key):
    """ Gets a dictionary and a key.
    Returns the value associated with the key, or None if key not in dictionary. """
    return dictionary[key] if key in dictionary.keys() else None
