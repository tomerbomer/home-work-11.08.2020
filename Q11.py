sentance = "I love to eat ice cream in the beach"

upperCase_sentance = [word.upper() for word in sentance.split()]

first_letter = [word[0] for word in sentance.split()]

third_letter = [word[2] for word in sentance.split() if len(word) >= 3]

word_count = [len(word) for word in sentance.split()]
